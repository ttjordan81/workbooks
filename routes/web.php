<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/workbooks', 'HomeController@workbook')->name('workbook');
Route::get('/my-projects', 'HomeController@myprojects')->name('myprojects');

Route::resource('course', 'CourseController');
Route::resource('folder', 'FolderController');
Route::resource('worksheet', 'WorksheetController');
Route::resource('worksheet-filled-out', 'WorksheetFilledOutController');

Route::get('/course/ql/{co}/{id}', 'SharedCourseContentController@unique_link');
Route::get('/folder/ql/{fo}/{id}', 'SharedFolderContentController@unique_link');
Route::get('/worksheet/ql/{wo}/{id}', 'SharedWorksheetContentController@unique_link');
Route::get('/worksheet-clone/{id}', 'WorksheetController@worksheet_clone');
Route::post('/updateEnrollment', 'WorksheetController@updateEnrollment');

// Handle routes or models not found methods. 
Route::fallback(function(){
    return response()->view('errors.404', [], 404);
})->name('fallback');
