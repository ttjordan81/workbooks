<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment_Status extends Model {

	protected $table = 'enrollment_status';
	public $timestamps = false;

	public function enrollment()
	{
		return $this->belongsTo('App\Enrollment', 'module_id');
	}	
	
}