<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'folders';

    // Make all attribute guarded
    protected $guarded = ['id'];

    public function course()
    {
        return $this->belongsTo('App\Course', 'course_id', 'id'); // method hasMany('App\Class', 'foreign_key', 'local_key')
    }

    public function worksheets()
    {
        return $this->hasMany('App\Worksheet', 'folder_id', 'id'); // method hasMany('App\Class', 'foreign_key', 'local_key')
    }

    public function worksheetsfilled()
    {
        return $this->hasMany('App\WorksheetFilledOut', 'folder_id', 'id'); // method hasMany('App\Class', 'foreign_key', 'local_key')
    }
}
