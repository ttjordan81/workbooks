<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Enrollment extends Model {

	protected $table = 'enrollment';
	public $timestamps = false;

	// Make all attribute guarded
	protected $guarded = ['id'];


	public function status()
	{
		return $this->belongsTo('App\Enrollment_Status', 'enrollment_status_id');
	}

	public function course()
	{
		return $this->belongsTo('App\Course', 'course_id');
	}

	public function folder()
	{
		return $this->belongsTo('App\Folder', 'folder_id');
	}	
	
	public function worksheet()
	{
		return $this->belongsTo('App\Worksheet', 'worksheet_id');
	}	


}