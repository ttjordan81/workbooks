<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FolderTypes extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'folder_types';

    // Make all attribute guarded
    protected $guarded = ['id'];
}
