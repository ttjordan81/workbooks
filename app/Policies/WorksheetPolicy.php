<?php

namespace App\Policies;

use App\Worksheet;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class WorksheetPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any courses.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the worksheet.
     *
     * @param  \App\User  $user
     * @param  \App\WorkSheet  $worksheet
     * @return mixed
     */
    public function view(User $user, Worksheet $worksheet)
    {
        // allow admin
        if($user->hasAnyRole(['superadmin'])) {
            return  true ;
        // allow if owner
        }else if($user->id === $worksheet->creator_user_id){
            return  true ;
        }
    }

    /**
     * Determine whether the user can create courses.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
        return $user->hasAnyRole(['superadmin', 'teacher', 'student']);
    }

    /**
     * Determine whether the user can update the folder.
     *
     * @param  \App\User  $user
     * @param  \App\WorkSheet  $workSheet
     * @return mixed
     */
    public function update(User $user, WorkSheet $workSheet)
    {
        //
        return $user->hasAnyRole(['superadmin', 'teacher']);
    }

    /**
     * Determine whether the user can delete the folder.
     *
     * @param  \App\User  $user
     * @param  \App\WorkSheet  $workSheet
     * @return mixed
     */
    public function delete(User $user, WorkSheet $workSheet)
    {
        //
        if($user->id === $workSheet->creator_user_id){
            return true;
        }elseif($user->hasAnyRole(['superadmin'])){
            return true;
        }
    }

    /**
     * Determine whether the user can restore the folder.
     *
     * @param  \App\User  $user
     * @param  \App\WorkSheet  $workSheet
     * @return mixed
     */
    public function restore(User $user, WorkSheet $workSheet)
    {
        //
        return $user->hasAnyRole(['superadmin', 'teacher']);
    }

    /**
     * Determine whether the user can permanently delete the folder.
     *
     * @param  \App\User  $user
     * @param  \App\WorkSheet  $workSheet
     * @return mixed
     */
    public function forceDelete(User $user, WorkSheet $workSheet)
    {
        //
        return $user->hasAnyRole(['superadmin', 'teacher']);
    }
}
