<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Folder;
use App\FolderTypes;
use Illuminate\Support\Facades\Auth;
use Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class FolderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:superadmin|teacher|student']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$this->authorize('create', Folder::class);

        //custom error message
        $messages = [
            //'course_id.required' => 'ID requrired!',
            'name.required' => 'Name requrired!',
        ];

        // form validation rules
        $rules = [
            // check if id exist, if not then warn user.
            //'course_id' => 'required',
            'name' => 'required',
        ];

        $this->validate($request, $rules, $messages);

        // we need to get the folder type id
        $folder_type =  FolderTypes::where('name', '=', $request->get('type'))->first();

        // load data object to be saved.
        $folder = new Folder();
        $folder->user_id = Auth::id();
        $folder->course_id = $request->get('course_id') != null ? $request->get('course_id') :  null ;
        $folder->name = $request->get('name');
        $folder->folder_types_id = $folder_type->id;
        
        // save data
        if ($folder->save()) {

            $folder->unique_link = md5($folder->id . 'folder');
            $folder->save();

            return response()->json([
                'name' => $folder->name,
                'id' => $folder->id,
                'unique_link' => $folder->unique_link
                ]);
        } else {
            return response()->json(['error' => 'Opps, something went wrong!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get current logged in user
        $user = Auth::user();

        // load course
       // $folder = Folder::with(['folders'])->find($id);
        
        $folder = Folder::find($id);
        if ($user->can('view', $folder)) {
            return view('Folder.show', array(
                'folder' => $folder,
            ));
        } else {
            abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get current logged in user
        $user = Auth::user();

        // load folder
        $folder = Folder::find($id);

        if ($user->can('view', $folder)) {
            return view('Folder.edit', array(
                'folder' => $folder,
            ));
        } else {
            abort('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //custom error message
        $messages = [
            'name.required' => 'Name needed!',
        ];

        // form validation rules
        $rules = [
            // check if id exist, if not then warn user.
            'name' => 'required',
        ];

        $this->validate($request, $rules, $messages);

        // load data object to be saved.
        $folder = Folder::findOrFail($id);

        // load field, then get data and save data
        $folder->name = $request->get('name');

        // save data
        if ($folder->save()) {
            Session::flash('success', 'Folder edits saved!');
            // return to from blade
            return redirect('/folder/'. $id);
        } else {
            Session::flash('success', 'No record created.');
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $folder = Folder::findOrFail($id);
        $folder->delete();
    
        Session::flash('success', 'Folder deleted!');
        return redirect('/home');
    }
}
