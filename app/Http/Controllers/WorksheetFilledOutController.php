<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\Folder;
use App\Worksheet as Worksheet;
use App\WorksheetFilledOut;
use App\Enrollment;
use Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class WorksheetFilledOutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       // dd($request);

        //custom error message
        $messages = [
            'folder_id.required' => 'Please select folder!',
        ];

        // form validation rules
        $rules = [
            'folder_id' => 'required',
        ];

        $this->validate($request, $rules, $messages);

       // dd($request);
       // dd($request->get('form_data'));

        //
        $AllInputData = $request->all();
        $toJSON = json_encode($AllInputData);
        $id = $request->get('worksheet_id');

        // run query to see if has been started and filled out,  if so return Worksheetfilledout
        $checkIfRecordExist = WorksheetFilledOut::where('id', '=', $id)
        ->where('user_id', '=', Auth::id())
        ->exists();
        
        //dd($checkIfRecordExist);
        //dd($request);
        // start logic check
        if($checkIfRecordExist){
            $worksheet = WorksheetFilledOut::where('id', '=', $id)->firstOrFail();

            WorksheetFilledOut::updateOrCreate(
                [
                    'user_id' => Auth::id(), 
                    'worksheet_id' => $worksheet->worksheet_id,
                ],
                [
                    'worksheet_id' => $worksheet->worksheet_id, 
                    'creator_user_id' => $worksheet->creator_user_id,
                    'user_id' => Auth::id(),
                    'folder_id' => $request->get('folder_id'),
                    //'menu_name' => $worksheet->menu_name,
                    'name' => $worksheet->name,
                    'unique_link' => $worksheet->unique_link,
                    'description' => $worksheet->description,
                    'form_data_serialize' => $toJSON,
                    'form_json' => $request->get('form_data'),
                ]
            );
        }else{
            /*
            * if not record has been filled out then return raw worksheet
            */ 

            $worksheet = Worksheet::where('id', '=', $id)->firstOrFail();
            //dd($worksheet);
            // we store new worksheet
            $worksheetFilledOut = new WorksheetFilledOut();
             
            $worksheetFilledOut->worksheet_id = $id;
            $worksheetFilledOut->creator_user_id = $worksheet->creator_user_id;
            $worksheetFilledOut->user_id = Auth::id();
            $worksheetFilledOut->folder_id = $request->get('folder_id');
           //$worksheetFilledOut->menu_name = $worksheet->menu_name;
            $worksheetFilledOut->name = $worksheet->name;
            $worksheetFilledOut->unique_link = $worksheet->unique_link;
            $worksheetFilledOut->description = $worksheet->description;
            $worksheetFilledOut->form_data_serialize = $toJSON;
            $worksheetFilledOut->form_json = $request->get('form_data');

            $worksheetFilledOut->save();
        }

        // get folder...
        //$worksheet_folder = Folder::where('id', '=' ,$worksheet->folder_id)->first();

        
        Session::flash('success', 'Worksheet saved!');
        return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WorksheetFilledOut  $worksheetFilledOut
     * @return \Illuminate\Http\Response
     */
    public function show(WorksheetFilledOut $worksheetFilledOut)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WorksheetFilledOut  $worksheetFilledOut
     * @return \Illuminate\Http\Response
     */
    public function edit(WorksheetFilledOut $worksheetFilledOut)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WorksheetFilledOut  $worksheetFilledOut
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorksheetFilledOut $worksheetFilledOut)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WorksheetFilledOut  $worksheetFilledOut
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        //dd($id);
        // get current logged in user
        $user = Auth::user();

        $worksheet = WorksheetFilledOut::findOrFail($id);
        // check policiy first, to see if user owns worksheet
        $this->authorize('delete', $worksheet);
        $worksheet->delete();

        if($user->hasRole('student')){
            Session::flash('status', 'Worksheet deleted!');
            return redirect('/home');          
        }else{
            Session::flash('status', 'Worksheet deleted!');
            return redirect('/worksheet');
        }
    }
}
