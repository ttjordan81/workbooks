<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Folder;
use App\Worksheet;
use App\WorksheetFilledOut;
use App\Enrollment;
use Illuminate\Support\Facades\Auth;
use Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;


class WorksheetController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->middleware(['role:superadmin|teacher']);
        //
        $user = Auth::user();

        // get all worksheets if admin
        if($user->hasAnyRole(['superadmin'])){
            $worksheets = Worksheet::all();
        // get only what user owns
        }else{
            $worksheets = Worksheet::where('creator_user_id', '=', Auth::id())->get();
        }

        return view('Worksheet.index', array(
            'worksheets' => $worksheets,
            'user' => $user
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->middleware(['role:superadmin|teacher']);
        //
        $course = Course::with(['folders'])->where('user_id', '=', Auth::id())->get();
        $course_list = Course::where('user_id', '=', Auth::id())->pluck('name','id');

        return view('Worksheet.create',
            array(
                'course' => $course,
                'course_list' => $course_list,
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('create', Worksheet::class);

        //custom error message
        $messages = [
            'name.required' => 'Title requrired!',
            //'menu_name.required' => 'Menu name requrired!',
            'description.required' => 'Description needed',
            //'form_json.required' => 'Form needs data!',
        ];

        // form validation rules
        $rules = [
            // check if id exist, if not then warn user.
            'name' => 'required',
            //'menu_name' => 'required',
            'description' => 'required',
            //'form_json' => 'required',
        ];

        $this->validate($request, $rules, $messages);

        // load data object to be saved.
        $worksheet = new Worksheet();
        $worksheet->creator_user_id	 = Auth::id();
        $worksheet->folder_id = $request->get('folder_id');
        //$worksheet->menu_name = $request->get('menu_name');
        $worksheet->name = $request->get('name');
        $worksheet->description = $request->get('description');
        $worksheet->form_json = $request->get('form_json');

        // save data
        if ($worksheet->save()) {
            // we create a hashed URL id, to send out, the value is the
            // new create id of the worksheet. 
            //$worksheet->unique_link = Hash::make($worksheet->id);
            $worksheet->unique_link = md5($worksheet->id);
            $worksheet->save();
            Session::flash('success', 'New worksheet was created!');
            // return to from blade
            return redirect('/worksheet');
            
        } else {
            Session::flash('error', 'Something went wrong, please contact support.');
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->middleware(['role:superadmin|teacher']);
        // get current logged in user
        $user = Auth::user();

        // load worksheet
        $worksheet = Worksheet::find($id);
        
        if ($user->can('view', $worksheet)) {
            return view('Worksheet.show', array(
                'worksheet' => $worksheet,
            ));
        } else {
            abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->middleware(['role:superadmin|teacher']);
        // get current logged in user
        $user = Auth::user();

        // load worksheet
        $worksheet = Worksheet::find($id);

        $course = Course::with(['folders'])->where('user_id', '=', Auth::id())->get();
        $course_list = Course::where('user_id', '=', Auth::id())->pluck('name','id');

		// pull all courses from course table, assign to $course_list variable.
		$folder_list = Folder::where('user_id', '=', Auth::id())->pluck('name','id');

        if ($user->can('view', $worksheet)) {
            return view('Worksheet.edit', array(
                'worksheet' => $worksheet,
                'folder_list' => $folder_list,
                'course' => $course,
                'course_list' => $course_list,
            ));
        } else {
            abort('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->middleware(['role:superadmin|teacher']);
        //
        $this->authorize('create', Worksheet::class);

        //custom error message
        $messages = [
            'name.required' => 'Title requrired!',
            //'menu_name.required' => 'Menu name requrired!',
            'description.required' => 'Description needed',
            //'form_json.required' => 'Form needs data!',
        ];

        // form validation rules
        $rules = [
            // check if id exist, if not then warn user.
            'name' => 'required',
            //'menu_name' => 'required',
            'description' => 'required',
            //'form_json' => 'required',
        ];
        
        $this->validate($request, $rules, $messages);

        // load data object to be saved.
        $worksheet = Worksheet::find($id);
        $worksheet->folder_id = $request->get('folder_id');
        //$worksheet->menu_name = $request->get('menu_name');
        $worksheet->name = $request->get('name');
        $worksheet->description = $request->get('description');
        $worksheet->form_json = $request->get('form_json');

        // save data
        if ($worksheet->save()) {
            // we create a hashed URL id, to send out, the value is the
            // new create id of the worksheet. 
            //$worksheet->unique_link = Hash::make($worksheet->id);
            //$worksheet->unique_link = md5($worksheet);
            $worksheet->save();

            // Update the enrollment, if a folder has been assigned.
            // we set the first arg to new Request which is nothing, it used
            // to statisfy the function, however, the second arg, is the data
            // we pass, $worksheet
            $this->updateEnrollment(new Request(), $worksheet);

            Session::flash('success', 'Worksheet was updated!');
            // return to from blade
            return redirect('/worksheet');
            
        } else {
            Session::flash('error', 'Something went wrong, please contact support.');
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // get current logged in user
        $user = Auth::user();

        $worksheet = Worksheet::findOrFail($id);
        // check policiy first, to see if user owns worksheet
        $this->authorize('delete', $worksheet);
        $worksheet->delete();

        if($user->hasRole('student')){
            Session::flash('status', 'Worksheet deleted!');
            return redirect('/home');          
        }else{
            Session::flash('status', 'Worksheet deleted!');
            return redirect('/worksheet');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function worksheet_clone(int $id)
    {
        $user = Auth::user();

        if($user->hasRole('student')){
            $worksheet = WorksheetFilledOut::where('id', '=', $id)->first();
            //dd($id);
            //dd($worksheet);
        }else{
            $worksheet = Worksheet::find($id);
        }

        // can't clone an empty worksheet, needs to be filled out and saved first
        if($worksheet == null){
            Session::flash('message', 'Worksheet needs to be filled out and saved first.');
            return back()->withInput();
        }
        //dd($worksheet);
        $clone = $worksheet->replicate();
        $clone->creator_user_id = Auth::id();
        $clone->name =  $clone->name . ' copy';
        //$clone->menu_name =  $clone->menu_name . ' copy';
        $clone->save();

        // we save again to get the new created object ID, and we hash off that.
        $clone->unique_link = md5($clone->id);
        $clone->save();

        if($user->hasRole('student')){
            Session::flash('success', 'Worksheet cloned!');
            return redirect('/worksheet/ql/wo/' . $clone->unique_link);          
        }else{
 
            Session::flash('success', 'Worksheet cloned!');
            return redirect('/worksheet');
        }
    }

    /**
     * Remove the specified resource from storage.
     * We use this function to push to data from a Route and
     * also, to call on it from another functions.
     *
     * @param  collection  $request
     * @param  var $worksheet // set to null
     */    
    public function updateEnrollment(Request $request, $worksheet = null)
    {
        // Both statements do the same thing, however, depending on where the
        // function is being executed from, then we want to choose which block to run.
        if($worksheet != null){
            Enrollment::where('worksheet_id', $worksheet->id)
            ->update(['folder_id' => $worksheet->folder_id]);
        }else{
            Enrollment::updateOrCreate(
                [
                    'worksheet_id' => $request->get('worksheet_id'),
                    'user_id' => Auth::id(),
                ],
                [
                    'folder_id' => $request->get('folder_id'),
                    'worksheet_id' => $request->get('worksheet_id'),
                    'user_id' => Auth::id(),
                ]
            ); 
            return response()->json(['folder' => 'saved']);
        }
    }

}
