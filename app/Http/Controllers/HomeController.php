<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Enrollment;
use App\Folder;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   

        $showObject = [];
        $cfw = Enrollment::where('user_id', '=',  Auth::id())->get();
        $folders = Folder::where('user_id', '=',  Auth::id())->get();

        return view('home', array(
            'cfw' => $cfw,
            'showObject' => $showObject,
            'folders' => $folders,
        ));
    }

    /**
     * Show the application dashboard for workbooks.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function workbook()
    {
        $showObject = [];
        $cfw = Enrollment::where('user_id', '=',  Auth::id())->get();
        $folders = Folder::where('user_id', '=',  Auth::id())->get();

        return view('workbook', array(
            'cfw' => $cfw,
            'showObject' => $showObject,
            'folders' => $folders,
        ));
    }


    /**
     * Show the application dashboard for workbooks.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function myprojects()
    {
        $showObject = [];
        $cfw = Enrollment::where('user_id', '=',  Auth::id())->get();
        $folders = Folder::where('user_id', '=',  Auth::id())->where('folder_types_id', '=', 2)->get();

        return view('myprojects', array(
            'cfw' => $cfw,
            'showObject' => $showObject,
            'folders' => $folders,
        ));
    }

}
