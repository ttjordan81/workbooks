<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Course;
use Illuminate\Support\Facades\Auth;
use Session;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;

class CourseController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:superadmin|teacher']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // get all courses if admin
        if($user->hasAnyRole(['superadmin'])){

            $courses = Course::all();

        // get only what user owns
        }else{
            $courses = Course::where('user_id', '=', Auth::id())->get();
        }

        return view('Course.index', array(
            'courses' => $courses,
            'user' => $user
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Course::class);
        //
        return view('Course.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->authorize('create', Course::class);

        //custom error message
        $messages = [
            //'menu_name.required' => 'Menu name requrired!',
            'name.required' => 'Name needed!',
            'description.required' => 'Description needed',
        ];

        // form validation rules
        $rules = [
            // check if id exist, if not then warn user.
            //'menu_name' => 'required',
            'name' => 'required',
            'description' => 'required',
        ];

        $this->validate($request, $rules, $messages);

        // load data object to be saved.
        $course = new Course();
        $course->user_id = Auth::id();
        //$course->menu_name = $request->get('menu_name');
        $course->name = $request->get('name');
        $course->description = $request->get('description');

        // save data
        if ($course->save()) {
            // we create a hashed URL id, to send out, the value is the
            // new create id of the course. 
            //$course->unique_link = Hash::make($course->id);
            $course->unique_link = md5($course->id);
            $course->save();
            Session::flash('success', 'Course edits saved!');
            // return to from blade
            return redirect('/course/'. $course->id);
        } else {
            Session::flash('success', 'No record created.');
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // get current logged in user
        $user = Auth::user();

        // load course
        $course = Course::with(['folders'])->find($id);
        
        if ($user->can('view', $course)) {
            return view('Course.show', array(
                'course' => $course,
            ));
        } else {
            abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // get current logged in user
        $user = Auth::user();

        // load course
        $course = Course::find($id);

        if ($user->can('view', $course)) {
            return view('Course.edit', array(
                'course' => $course,
            ));
        } else {
            abort('404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //custom error message
        $messages = [
            //'menu_name.required' => 'Menu name requrired!',
            'name.required' => 'Name needed!',
            'description.required' => 'Content needed',
        ];

        // form validation rules
        $rules = [
            // check if id exist, if not then warn user.
            //'menu_name' => 'required',
            'name' => 'required',
            'description' => 'required',
        ];

        $this->validate($request, $rules, $messages);

        // load data object to be saved.
        $course = Course::findOrFail($id);

        // load field, then get data and save data
        //$course->menu_name = $request->get('menu_name');
        $course->name = $request->get('name');
        $course->description = $request->get('description');

        // save data
        if ($course->save()) {
            Session::flash('success', 'Course edits saved!');
            // return to from blade
            return redirect('/course/'. $id);
        } else {
            Session::flash('success', 'No record created.');
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();
    
        Session::flash('success', 'Course deleted!');
        return redirect('/course');
    }
}
