<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\Folder;
use App\Worksheet;
use App\Enrollment;

class SharedCourseContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:superadmin|teacher|student']);
    }

    /**
     * If users have correct link then Display the specified unique_link.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unique_link($type, $id)
    {   
        // check if the link as the proper string, if not send to abort 404 page
        if ($type != 'co') return abort(404);

        // get current logged in user
        $user = Auth::user();
        $course = Course::where('unique_link', '=', $id)->firstOrFail();
        $folders = Folder::where('course_id', '=' ,$course->id)->get();
        $cfw = Enrollment::where('user_id', '=',  Auth::id())->get();

        //dd($id);
        // checking to see if user owns this workshee, if so, then do not enroll them into their own worksheets.
        if(Auth::id() !== $course->user_id){

            if($type === 'co'){
                foreach ($course->worksheets as $data) {
                    Enrollment::updateOrCreate(
                        [
                            'worksheet_id' => $data->id,
                            'user_id' => Auth::id(),
                        ],
                        [
                            'course_id' => $course->id,
                            'folder_id' => $data->folder_id,
                            'worksheet_id' => $data->id,
                            'user_id' => Auth::id(),
                        ]
                    );
                }
            }
        }
            
        if($user->hasRole('student')){

            $getCourse = Course::with(['folders.worksheets'])->where('unique_link', $id)->first();

            return view('Course.shared', array(
                'course' => $getCourse
            ));

        }else{
            return view('Course.shared', array(
                'course' => $course,
                'folders' => $folders,
                'cfw' => $cfw,
            ));
        }
    }
}
