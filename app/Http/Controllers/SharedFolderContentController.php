<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\Folder;
use App\Enrollment;
use App\WorksheetFilledOut;

class SharedFolderContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:superadmin|teacher|student']);
    }

    /**
     * If users have correct link then Display the specified unique_link.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unique_link($type, $id)
    {
        // check if the link as the proper string, if not send to abort 404 page
        if ($type != 'fo') return abort(404);

        // get current logged in user
        $user = Auth::user();
        $folder = Folder::where('unique_link', '=', $id)->firstOrFail();
        $folder_course = Course::where('id', '=' ,$folder->course_id)->first();
        $enrolled = Enrollment::where('user_id', '=' , Auth::id())->where('folder_id', '=', $folder->id)->get();
        $worksheets = WorksheetFilledOut::where('user_id', '=' , Auth::id())->where('folder_id', '=', $folder->id)->get();

        // checking to see if user owns this workshee, if so, then do not enroll them into their own worksheets.
        if(Auth::id() !== $folder->user_id){

            if($type === 'fo'){
                foreach ($folder->worksheets as $data) {
                    Enrollment::updateOrCreate(
                        [
                            'worksheet_id' => $data->id,
                            'user_id' => Auth::id(),
                        ],
                        [
                            'folder_id' => $data->folder_id,
                            'worksheet_id' => $data->id,
                            'user_id' => Auth::id(),
                        ]
                    );
                }
            }

            // we check if the is a student, then we add folder to enrollment
            if($user->hasRole('student')){
                Enrollment::updateOrCreate(
                    [
                        'folder_id' => $folder->id,
                        'user_id' => Auth::id(),
                    ],
                    [
                        'folder_id' => $folder->id,
                        'user_id' => Auth::id(),
                    ]
                );
            }
        }

        return view('Folder.shared', array(
            'folder' => $folder,
            'folder_course' => $folder_course,
            'enrolled' => $enrolled,
            'worksheets' => $worksheets,
        ));
    }
}
