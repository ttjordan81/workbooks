<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Course;
use App\Folder;
use App\Worksheet;
use App\Enrollment;
use App\WorksheetFilledOut;

class SharedWorksheetContentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(['role:superadmin|teacher|student']);
    }

    /**
     * If users have correct link then Display the specified unique_link.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function unique_link($type, $id)
    {
        // check if the link as the proper string, if not send to abort 404 page
        if ($type != 'wo') return abort(404);

        // get current logged in user
        $user = Auth::user();

        // run query to see if has been started and filled out,  if so return Worksheetfilledout
        $checkIfRecordExist = WorksheetFilledOut::where('unique_link', '=', $id)
        ->where('user_id', '=', $user->id)
        ->exists();
        // start logic check
        if($checkIfRecordExist){
            $worksheet = WorksheetFilledOut::where('unique_link', '=', $id)->firstOrFail();
        }else{
            // if not record has been filled out then return raw worksheet
            $worksheet = Worksheet::where('unique_link', '=', $id)->firstOrFail();
        }

        // get the folder that match where clause
        $worksheet_folder = Folder::where('id', '=' , $worksheet->folder_id)
       // ->where('user_id', '=', $user->id)
        ->first();

        // get all the worksheet folders
        $folders = Folder::where('user_id', '=', Auth::id())
        ->where('user_id', '=', $user->id)
        ->pluck('name','id');

        // checking to see if user owns this workshee, if so, then do not enroll them into their own worksheets.
        if(Auth::id() !== $worksheet->creator_user_id){

            // we need to update the enrollement
            if($type === 'wo'){

                if($user->hasRole('student') && $worksheet->getTable() == 'worksheet_filled_out'){

                    //dd($worksheet);
                    Enrollment::updateOrCreate(
                        [
                            'worksheet_filled_id' => $worksheet->id,
                            'worksheet_id' => $worksheet->worksheet_id,
                            'user_id' => Auth::id(),
                        ],
                        [
                            'worksheet_filled_id' => $worksheet->id,
                            'worksheet_id' => $worksheet->worksheet_id,
                            'user_id' => Auth::id(),
                        ]
                    );
                }else{
                    Enrollment::updateOrCreate(
                        [
                            'worksheet_id' => $worksheet->id,
                            'user_id' => Auth::id(),
                        ],
                        [
                            'worksheet_id' => $worksheet->id,
                            'user_id' => Auth::id(),
                        ]
                    );
                }

            }
        }

        // We check if the form has alraedy been filled out, if so then return the data.
        $checkIfFilledOut = $this->getFilledOutForm($worksheet->id);

        return view('Worksheet.shared', array(
            'worksheet' => $worksheet,
            'worksheet_folder' => $worksheet_folder,
            'folders' => $folders,
            'data_submitted' => $checkIfFilledOut != null ? $checkIfFilledOut->form_data_serialize : '""'
        ));
    }

    public function getFilledOutForm($id){
        
       $data =  WorksheetFilledOut::where('user_id', '=', Auth::id())
        ->where('worksheet_id', '=', $id)
        ->first();

        return $data;
    }

}
