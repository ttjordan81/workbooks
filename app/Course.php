<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'courses';

    // Make all attribute guarded
    protected $guarded = ['id'];

    public function folders()
    {
        return $this->hasMany('App\Folder', 'course_id', 'id'); // method hasMany('App\Class', 'foreign_key', 'local_key')
    }

    /**
     * Get all of the posts for the country.
     */
    public function worksheets()
    {
        return $this->hasManyThrough(
            'App\Worksheet',
            'App\Folder', 
            'course_id', // Foreign key on users table...
            'folder_id', // Foreign key on posts table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
    }

}
