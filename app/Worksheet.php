<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worksheet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'worksheets';

    // Make all attribute guarded
    protected $guarded = ['id'];

    public function folder()
    {
        return $this->belongsTo('App\Folder', 'folder_id', 'id'); // method hasMany('App\Class', 'foreign_key', 'local_key')
    }
}
