<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use App\User;

class CourseTest extends DuskTestCase
{
    /**
     * A Dusk test for checking CAN a admin
     * user create an course.
     *
     * @return void
     */
    public function testCreateCourse()
    {

        print "\n\n ** Started Course Model testing ** \n";


        $user = User::find(1);
        // checks for the role of admin
        if ($user->hasAnyRole('superadmin')) {
            // init broswer 
            $this->browse(function (Browser $browser) use ($user) {
                // execute out actions
                $browser->loginAs($user)
                ->visit('/course/create')
                ->type('name', 'Example name goes here')
                //->type('menu_name', 'Example menu name goes here')
                ->type('description', 'Example course description')
                ->press('Create')
                ->assertSee('Shared link:');
            });
        }

        print "\nPASSED: Admin user can authenicate, and create a course";
    }
}
