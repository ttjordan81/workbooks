<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test see if home is there and loads.
     *
     * @return void
     */
    public function testHome()
    {
        print "\n\n ** Started Reponse 200 Testing ** \n";

        $response = $this->get('/');

        $response->assertStatus(200);
        print "\nPASSED: Home page ";
    }
    /**
     * A basic test see if login is there and loads.
     *
     * @return void
     */
    public function testLogin()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
        print "\nPASSED: Login page";
    }
    /**
     * A basic test see if register is there and loads.
     *
     * @return void
     */
    public function testRegister()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
        print "\nPASSED: Register page";
    }
}
