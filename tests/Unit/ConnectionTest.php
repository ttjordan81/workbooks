<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use DB;

class ConnectionTest extends TestCase
{
    /**
     * A basic test see out database is connected.
     *
     * @return void
     */
    public function testDBConnection()
    {
        print "\n\n ** Started Connection Testing **\n";

        try {
            DB::connection()->getPdo();

            $name = DB::connection()->getDatabaseName();
            $DBNames = ['workbooks', 'PROD', 'STAGING'];

            if($name){

                $this->assertContains($name, $DBNames);
                print "\nPASSED: Successfully connected to the DB: " . DB::connection()->getDatabaseName() . "\n";
    
            }else{
                die("FAILED:Could not find the database. Please check your configuration. \n");
  
            }
        } catch (\Exception $e) {
            die("FAILED: Could not open connection to database server.  Please check your configuration. \n");
        }
    }

}
