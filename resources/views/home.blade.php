@extends('layouts.app')

@section('content')

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif

<p>Want to create your own <strong>Workbooks?</strong> Become a course instructor <a href="#">Sign up!</a></p>

<p>Maybe add more promo stuff here, or dates to webinars, promo videos, etc...</p>

@if(Auth::user()->hasRole('superadmin') || Auth::user()->hasRole('teacher'))
    <a href="{{ route('course.index') }}">View all Courses</a> | 
    <a href="{{ route('course.create') }}" class="">Create course </a>
@endif



@endsection


@section('scripts')

<script type="text/javascript">

function addFolder() {

    if ($('#add_name').length == 0) {
        $('#folder_container').append(
            '<div class="col col-sm-2 col-md-2 col-lg-1"><i class="fa fa-folder fa-4x"></i><input id="add_name" type="text" name="name" class="col-md-12 p-0" autocomplete="off"/><button class="col-md-6 btn btn-sm btn-primary mt-1" onclick="saveName(this)"><i class="fa fa-save"></i></button><button class="col-md-6 btn btn-sm btn-danger mt-1" onclick="removeInput(this)"><i class="fa fa-times-circle"></i></button></div>'
        );

        $("#folder_container input").focus();
    }
}

function removeInput(){
    $('#add_name').parent().remove('');
}

/********************************************************
 * Save the YouTube URL to the database
 ********************************************************/
function saveName(data) {

    var folderName = $(data.parentElement).find('input').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        method: "POST",
        url: "/folder",
        dataType: 'json',
        data: {
            name: folderName
        },
        async: false,
        success: function(data) {
            $('#add_name').parent().remove('');
            
            $('#folder_container').append('<div id="'+ data.id +'" class="col col-sm-2 col-md-2 col-lg-1"><i class="fa fa-folder fa-4x"></i> <a href="/folder/'+ data.id +'">'+ data.name +'</a> </div>');
            console.log(data);
        }
    });

}
</script>
@stop
