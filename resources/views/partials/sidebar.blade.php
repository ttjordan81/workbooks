<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">
                        
                        {{ Auth::user()->name }}
                        
                        </span>
                    </a>
                    
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>

        @if(Auth::user()->hasRole('superadmin') || Auth::user()->hasRole('teacher'))
        <li class="{{ Request::segment(1) == 'home' ? 'active' : ''}}">
            <a href="{{ route('home') }}"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
        </li>
        
        <li class="active">
            <a href="#" aria-expanded="true"><i class="fa fa-institution"></i> <span class="nav-label">Teacher</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
                <li><a href="{{ route('course.index') }}">Courses</a></li>
                <li><a href="{{ route('worksheet.index') }}">All Worksheets</a></li>
            </ul>
        </li>
        @endif

        @if(Auth::user()->hasRole('student'))
        <li class="active">
            <a href="#" aria-expanded="true"><i class="fa fa-graduation-cap"></i> <span class="nav-label">Student</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level collapse in" aria-expanded="true" style="">
                <li class="{{ Request::segment(1) == 'workbooks' ? 'active' : ''}}"><a href="{{ route('workbook') }}">Workbooks</a></li>
                <li class="{{ Request::segment(1) == 'projectfolders' ? 'active' : ''}}"><a href="{{ route('myprojects') }}">My Projects</a></li>
 
            </ul>
        </li>
        @endif

        </ul>

    </div>
</nav>