@extends('layouts.app')

@section('content')

@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif


@if(Auth::user()->hasRole('student'))
    <a href="{{ route('workbook') }}">Go to Workbooks</a>

    <hr>

    <h4 class="mb-4"> Folders 
        <button class="btn btn-primary btn-sm" onclick="addFolder()"><i class="fa fa-folder"></i> +</button>
    </h4>

    <div id="folder_container" class="row overflow-auto" >
        @foreach($folders as $folder)
        <div id="{{ $folder->id }}" class="col col-sm-2 col-md-2 col-lg-1">
            <i class="fa fa-folder fa-4x"></i>
            <a href="/folder/{{ $folder->id }}" class="block">{{ $folder->name }}</a>
        </div>
        @endforeach
    </div>  
@endif

@endsection


@section('scripts')

<script type="text/javascript">

function addFolder() {

    if ($('#add_name').length == 0) {
        $('#folder_container').append(
            '<div class="col col-sm-2 col-md-2 col-lg-1"><i class="fa fa-folder fa-4x"></i><input id="add_name" type="text" name="name" class="col-md-12 p-0" autocomplete="off"/><button class="col-md-6 btn btn-sm btn-primary mt-1" onclick="saveName(this)"><i class="fa fa-save"></i></button><button class="col-md-6 btn btn-sm btn-danger mt-1" onclick="removeInput(this)"><i class="fa fa-times-circle"></i></button></div>'
        );

        $("#folder_container input").focus();
    }
}

function removeInput(){
    $('#add_name').parent().remove('');
}

/********************************************************
 * Save the YouTube URL to the database
 ********************************************************/
function saveName(data) {

    var folderName = $(data.parentElement).find('input').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        method: "POST",
        url: "/folder",
        dataType: 'json',
        data: {
            name: folderName,
            type: 'project_folders'
        },
        async: false,
        success: function(data) {
            $('#add_name').parent().remove('');
            
            $('#folder_container').append('<div id="'+ data.id +'" class="col col-sm-2 col-md-2 col-lg-1"><i class="fa fa-folder fa-4x"></i> <a href="/folder/'+ data.id +'">'+ data.name +'</a> </div>');
            console.log(data);
        }
    });

}
</script>
@stop
