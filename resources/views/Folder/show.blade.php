@extends('layouts.app')

@section('content')

<div class="ibox ">
    <div class="ibox-title">
        <ol class="breadcrumb">

            @if($folder->course == null)
            <li class="breadcrumb-item">Student  </li>
            @elseif(Auth::user()->hasRole('superadmin') || Auth::user()->hasRole('teacher'))
            <li class="breadcrumb-item">Teacher  </li>
            <li class="breadcrumb-item">
                <a href="/course/{{ $folder->course->id }}">Course - <b>{{ $folder->course->name }}</b></a>
            </li>
            @endif
            <li class="breadcrumb-item">
                <a href="/folder/{{ $folder->id }}">Folder - <strong>{{ $folder->name }}</strong></a>
            </li>
        </ol>

        @can('create', App\Folder::class)
        <div class="ibox-tools">
            
            @if(Auth::user()->hasRole('superadmin') || Auth::user()->hasRole('teacher') )
                <a href="/worksheet/create" class="btn btn-primary btn-xs">Create new worksheet</a>
            @endif

            <a href="/folder/{{ $folder->id }}/edit" class="btn btn-primary btn-xs">Edit Folder</a>

        </div>
        @endcan

    </div>

    <div class="ibox-content">
        
        <h1> {{ $folder->name }} </h1>

      
            @if(Auth::id() === $folder->user_id)
            <p><b>Shared link:</b> <br>
                <input type="text" value="{{ url('folder/ql/fo') }}/{{ $folder->unique_link }}" id="myInput"
                    class="col-md-12 mb-2" />

                <a class="btn btn-success btn-sm" href="{{ url('folder/ql/fo') }}/{{ $folder->unique_link }} "
                    target="_blank"><i class="fa fa-link"></i> Review</a>

                <button class="btn btn-success btn-sm" onclick="copyField()">Copy link</button>
            </p>
            @endif
      

        <hr>

        <h4> Worksheets:</h4>

            @foreach($folder->worksheets as $worksheet)
                @if($worksheet->creator_user_id == Auth::id() || Auth::user()->hasRole('superadmin'))
                    <a href="/worksheet/{{ $worksheet->id }}">{{ $worksheet->name }}</a> <br>
                @endif
            @endforeach

        @if( Auth::user()->hasRole('student'))
            @foreach($folder->worksheetsfilled as $worksheet)
                <a href="/worksheet/ql/wo/{{ $worksheet->unique_link }}">{{ $worksheet->name }}</a> <br>
            @endforeach
        @endif

    </div>
</div>

@endsection

@section('scripts')


<script type="text/javascript">
function copyField() {
    /* Get the text field */
    var copyText = document.getElementById("myInput");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
}

function addFolder() {

    if ($('#add_name').length == 0) {
        $('#folder_container').append(
            '<div class="col-md-2"><i class="far fa-folder fa-4x"></i><input id="add_name" type="text" name="name" class="col-md-12 p-0" autocomplete="off"/><button class="col-md-6 btn btn-sm btn-primary mt-1" onclick="saveName(this)">s</button><button class="col-md-6 btn btn-sm btn-danger mt-1" onclick="removeInput(this)">c</button></div>'
        );

        $("#folder_container input").focus();

    }
}

function removeInput(){
    $('#add_name').parent().remove('');
}

/********************************************************
 * Save the YouTube URL to the database
 ********************************************************/
function saveName(data) {

    var folderName = $(data.parentElement).find('input').val()
    var course_id = '{{ $folder->id }}';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        method: "POST",
        url: "/folder",
        dataType: 'json',
        data: {
            course_id: course_id,
            name: folderName
        },
        async: false,
        success: function(data) {
            $('#add_name').parent().remove('');
            
            $('#folder_container').append('<div id="'+ data.id +'" class="col-md-2"><i class="far fa-folder fa-4x"></i> '+ data.name +' </div>');
            console.log(data);
        }
    });

}
</script>
@stop