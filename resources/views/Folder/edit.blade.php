@extends('layouts.app')

@section('content')

<div class="ibox-title">
    <h5>Edit Folder</h5>

    <div class="ibox-tools">
        <!-- Button trigger modal --> 
        <button type="button" class="btn btn-primary btn-xs float-right" data-toggle="modal" data-target="#exampleModal">
            [delete]
        </button>
    </div>
</div>

<div class="ibox-content">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }} | <a href="/departments" style="color:white;text-decoration: underline;"><i class="fa fa-list"></i> View all Departments</a>
        </div>
    @endif
    {{ Form::model($folder, ['route' => array('folder.update', $folder->id), 'class' => 'form-horizontal form-label-left', 'method' => 'PUT'] ) }}


    <div class="form-group">
        {!! Form::label('name', 'Folder title *', ['class' => 'control-label col-md-3 col-sm-3
        col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control col-xs-12',
            'required' => 'required']) !!}
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="{{ URL::previous() }}" class="btn btn-primary">Cancel</a>
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>

    {{ Form::close() }}

</div>






<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Are you sure you want to delete!
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::model($folder, ['route' => array('folder.destroy', $folder->id), 'method' => 'DELETE'] ) }}
                <button type="submit" class="btn btn-danger">[Yes, I'm sure!]</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection