@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

               

                <div class="card-header"><a href="/home">Dashboard</a> > Course
                    @can('create', App\Course::class)
                    
                    <div class="col-md-6">
                            <li> <a href="/course/create" class="float-right">Create new course </a> </li>
                            <li> <a href="/worksheet">View all Worksheets</a> </li>
                            <li> <a href="/worksheet/create" class="">Create worksheet </a> </li>
                        </div>
                    @endcan
                </div>
                <div class="card-body">
                    @if( $courses->isEmpty() == false)
                        @foreach($courses as $course)
                        <a href="/course/{{ $course->id }}">{{ $course->name }}</a> <br>
                        @endforeach
                    @else
                        @can('create', App\Course::class)
                        No course create yet!  <a href="/course/create">Create new course </a>
                        @endcan
                    @endif
                </div>


            </div>
        </div>
    </div>
</div>
@endsection