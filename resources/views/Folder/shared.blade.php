@extends('layouts.app')

@section('content')

<div class="ibox ">
    <div class="ibox-title">
        <h4>Folder <small>some text here...</small></h4>
    </div>

    <div class="ibox-content">
        
        <div id="jstree1" class="jstree jstree-1 jstree-default" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="j1_20" aria-busy="false">

            {{ $folder->name }}
        
            <ul role="group" class="jstree-children">
                @foreach($folder->worksheets as $worksheet)
                <li role="treeitem" data-jstree="&quot;type&quot;:&quot;img&quot;}" aria-selected="false" aria-level="3" aria-labelledby="j1_{{ $worksheet->id }}_anchor" id="j1_{{ $worksheet->id }}" class="jstree-node text-navy jstree-leaf jstree-last">
                    <i class="jstree-icon jstree-ocl" role="presentation"></i>
                    <a class="jstree-anchor" href="/worksheet/ql/wo/{{ $worksheet->unique_link }}" tabindex="-1" id="j1_{{ $worksheet->id }}_anchor">
                        <i class="fa fa-list" role="presentation"></i>
                        {{ $worksheet->name }}
                    </a>
                </li>
                @endforeach
            </ul>

        </div>

    </div>
</div>


@endsection