@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="/home">Dashboard</a> </b>
                    <a href="{{ URL::previous() }}" class="float-right">Cancel</a>
                </div>

                <div class="card-body">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }} | <a href="/departments" style="color:white;text-decoration: underline;"><i class="fa fa-list"></i> View all Departments</a>
                        </div>
                    @endif

                    {{ Form::open(['route' => 'course.store', 'class' => 'form-horizontal form-label-left', 'method' => 'POST']) }}   

                    <!---
					<div class="form-group">
						{!! Form::label('name', 'Course menu name *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::text('menu_name', null, ['id' => 'menu_name', 'class' => 'form-control col-xs-12', 'required' => 'required']) !!}
						</div>
                    </div>
                    -->
                    
                    <div class="form-group">
						{!! Form::label('name', 'Course title *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control col-xs-12', 'required' => 'required']) !!}
						</div>
                    </div>

                    <div class="form-group">
						{!! Form::label('name', 'Description *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
						<div class="col-md-6 col-sm-6 col-xs-12">
							{!! Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control col-xs-12', 'required' => 'required']) !!}
						</div>
                    </div>
                
                    <div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
							<a href="{{ URL::previous() }}" class="btn btn-primary">Cancel</a>
							<button type="submit" class="btn btn-success">Create</button>
						</div>
					</div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection