@extends('layouts.app')

@section('content')

<div class="ibox-title">
    <h5>{{ $course->name }}</h5>

    @can('create', App\Course::class)
    <div class="ibox-tools">
        <!-- Button trigger modal -->        
        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#deleteModal">
            [delete]
        </button>
    </div>
    @endcan
</div>

<div class="ibox-content">

{{ Form::model($course, ['route' => array('course.update', $course->id), 'class' => 'form-horizontal form-label-left', 'method' => 'PUT'] ) }}
                 
    <div class="form-group">
        {!! Form::label('name', 'Course title *', ['class' => 'control-label col-md-3 col-sm-3
        col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('name', null, ['id' => 'name', 'class' => 'form-control col-xs-12',
            'required' => 'required']) !!}
        </div>
    </div>
``<!--
    <div class="form-group">
        {!! Form::label('name', 'Course menu name *', ['class' => 'control-label col-md-3 col-sm-3
        col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::text('menu_name', null, ['id' => 'menu_name', 'class' => 'form-control col-xs-12',
            'required' => 'required']) !!}
        </div>
    </div>
    -->

    <div class="form-group">
        {!! Form::label('name', 'Description *', ['class' => 'control-label col-md-3 col-sm-3
        col-xs-12']) !!}
        <div class="col-md-6 col-sm-6 col-xs-12">
            {!! Form::textarea('description', null, ['id' => 'description', 'class' => 'form-control
            col-xs-12', 'required' => 'required']) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
            <a href="{{ URL::previous() }}" class="btn btn-primary">Cancel</a>
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </div>

{{ Form::close() }}
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Deleting Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            Warning: By deleting this course, you will be deleting all folders, and worksheets. This process is irreversible. Are you sure you want to delete this course?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::model($course, ['route' => array('course.destroy', $course->id), 'method' => 'DELETE'] ) }}
                <button type="submit" class="btn btn-danger">[Yes, I'm sure!]</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection