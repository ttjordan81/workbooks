@extends('layouts.app')

@section('content')

<div class="ibox ">
    <div class="ibox-title">
        <h4>Course</h4>
    </div>

    <div class="ibox-content">
        
    <h2>{{ $course->name }}</h2>
    <div id="jstree1">
    <ul role="group" class="jstree-children">
        @foreach($course->folders as $folder)
        <li aria-expanded="true" class="jstree-node  jstree-open">{{ $folder->name }}
        <ul>
            @foreach($folder->worksheets as $worksheet)
            <li data-jstree='{"icon":"fa fa-list"}' aria-selected="false" class="jstree-node text-navy jstree-leaf jstree-last">
                <a class="jstree-anchor" href="/worksheet/ql/wo/{{ $worksheet->unique_link }}"> 
                    {{ $worksheet->name }}
                </a>
            </li>


            @endforeach
        </ul>
        </li>
        @endforeach
    </ul>
    </div>




    </div>
</div>



@endsection

@section('scripts')

<script src="/js/plugins/jsTree/jstree.min.js"></script>

<script>
    $(document).ready(function(){

        $('#jstree1').jstree({
            'core' : {
                'check_callback' : true
            },
            'plugins' : [ 'types', 'dnd' ],
            'types' : {
                'default' : {
                    'icon' : 'fa fa-folder'
                },
                'html' : {
                    'icon' : 'fa fa-file-code-o'
                },
                'svg' : {
                    'icon' : 'fa fa-file-picture-o'
                },
                'css' : {
                    'icon' : 'fa fa-file-code-o'
                },
                'img' : {
                    'icon' : 'fa fa-file-image-o'
                },
                'js' : {
                    'icon' : 'fa fa-file-text-o'
                }

            },
            'dnd': {
            "is_draggable": function (node) {
                return false;  // stop draggable action
            }
        },
        }).bind("select_node.jstree", function (e, data) {
            var href = data.node.a_attr.href;
            var parentId = data.node.a_attr.parent_id;
            if(href == '#'){
                return '';
            }
            
            window.open(href);
        });
    });
</script>

@stop