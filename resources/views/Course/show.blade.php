@extends('layouts.app')

@section('content')

<div class="ibox-title">
    <h5>{{ $course->name }}</h5>

    @can('create', App\Course::class)
    <div class="ibox-tools">
        
        <a href="/course/{{ $course->id }}/edit" class="btn btn-primary btn-xs">Edit Course</a>
    </div>
    @endcan
</div>

<div class="ibox-content">
    <!--
    <p>
        <b>Menu Name:</b> <br>
        {{ $course->menu_name }}
    </p>
    -->
    <p>
        <b>Description:</b> <br>
        {!! nl2br($course->description) !!}
    </p>

    <p>
        <b>Shared link:</b> <br>

        @if(count($course->worksheets) == 0)
            Before sharing can done, you need to assign a <button class="btn btn-primary btn-xs" onclick="addFolder()"><i class="fa fa-folder"></i> Folder</button> , then a <a href="{{ url('/worksheet') }}">Worksheet</a> to the folder.
        @else
        <input type="text" value="{{ url('course/ql/co') }}/{{ $course->unique_link }}" id="myInput" class="col-md-12 mb-2" />

        <a class="btn btn-success btn-sm" href="{{ url('course/ql/co') }}/{{ $course->unique_link }} " target="_blank"><i class="fa fa-link"></i> Review</a>

        <button class="btn btn-success btn-sm" onclick="myFunction()">Copy link</button>

        @endif
    </p>

    <hr>

    <h4 class="mb-4"> Folders <small>[  add folder to this course ]</small>
    <button class="btn btn-primary btn-sm" onclick="addFolder()"><i class="fa fa-folder"></i> +</button>
    </h4>

    <div id="folder_container" class="row">
    @foreach($course->folders as $folder)

    @if($folder->folder_types_id == 1)
    <div id="{{ $folder->id }}" class="col col-sm-2 col-md-2 col-lg-1">
        <i class="fa fa-folder fa-4x"></i>
        <a href="/folder/{{ $folder->id }}" class="block">{{ $folder->name }}</a>
    </div>
    @endif


    @endforeach
    </div> 

</div>

@endsection

@section('scripts')


<script type="text/javascript">
function myFunction() {
    /* Get the text field */
    var copyText = document.getElementById("myInput");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
}

function addFolder() {

    if ($('#add_name').length == 0) {
        $('#folder_container').append(
            '<div class="col col-sm-2 col-md-2 col-lg-1"><i class="fa fa-folder fa-4x"></i><input id="add_name" type="text" name="name" class="col-md-12 p-0" autocomplete="off"/><button class="col-md-6 btn btn-sm btn-primary mt-1" onclick="saveName(this)"><i class="fa fa-save"></i></button><button class="col-md-6 btn btn-sm btn-danger mt-1" onclick="removeInput(this)"><i class="fa fa-times-circle"></i></button></div>'
        );

        $("#folder_container input").focus();
    }
}

function removeInput(){
    $('#add_name').parent().remove('');
}

/********************************************************
 * Save to the database
 ********************************************************/
function saveName(data) {

    var folderName = $(data.parentElement).find('input').val()
    var course_id = '{{ $course->id }}';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        method: "POST",
        url: "/folder",
        dataType: 'json',
        data: {
            course_id: course_id,
            name: folderName,
            type: 'course_folders'
        },
        async: false,
        success: function(data) {
            $('#add_name').parent().remove('');
            
            $('#folder_container').append('<div id="'+ data.id +'" class="col col-sm-2 col-md-2 col-lg-1""><i class="fa fa-folder fa-4x"></i> <a href="/folder/'+ data.id +'" class="block">'+ data.name +'</a> </div>');
            console.log(data);
        }
    });

}
</script>
@stop