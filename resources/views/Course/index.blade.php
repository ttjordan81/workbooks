@extends('layouts.app')

@section('content')

<div class="ibox-title">
    <h5>All courses assigned to this account </h5>
    @can('create', App\Course::class)
    <div class="ibox-tools">
        <a href="/course/create" class="btn btn-primary btn-xs">Create new course</a>
    </div>
    @endcan
</div>

<div class="ibox-content">

@if( $courses->isEmpty() == false)
    <div class="table-responsive">
        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
            <table id="DataTables_Table" class="table table-striped table-bordered table-hover dataTables-example" style="width:100%">
                <thead>
                    <tr>
                        <th class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Name</th>
                        <th class="text-center"># of Folders</th>
                        <th class="text-center"># of Worksheets</th>
                        <th class="text-right">View/Edit</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach($courses as $course)
                        <tr>
                            <td class="project-title">
                                <a href="/course/{{ $course->id }}">{{ $course->name }}</a>
                            </td>
                            <td class="text-center">{{ count($course->folders) }}</td>
                            <td class="text-center">{{ count($course->worksheets) }}</td>
                            <td class="project-actions">
                                <a href="/course/{{ $course->id }}" class="btn btn-white btn-sm"><i class="fa fa-folder"></i> View </a>
                                <a href="/course/{{ $course->id }}/edit" class="btn btn-white btn-sm"><i class="fa fa-pencil"></i> Edit </a>
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>

    @else
    @can('create', App\Course::class)
    No course create yet!  <a href="/course/create">Create new course </a>
    @endcan
@endif
</div>

@endsection

@section('scripts')
    <!-- Pages specific scripts -->
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('#DataTables_Table').DataTable({
                pageLength: 10,
                responsive: true,
            });

        });

    </script>
@stop