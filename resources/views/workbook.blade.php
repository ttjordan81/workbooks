@extends('layouts.app')

@section('content')

<div class="ibox-title">
    <h5>View all your courses, folders, and worksheets</h5>
</div>

<div class="ibox-content">

    <div class="table-responsive">
        <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
            <table id="DataTables_Table" class="table table-striped table-bordered table-hover dataTables-example" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>You have access to</th>
                    </tr>
                </thead>
                <tbody>
                   
                    <?php 
                        $i = 0; 
                        $showCourseObject = [];
                    ?>

                    @foreach($cfw as $data )
                    <tr>
                        @if( $data->course != null )

                            @if(!in_array($data->course_id, $showCourseObject ))
                                <?php array_push($showCourseObject, $data->course_id); ?>
                                <td><a href="/course/ql/co/{{ $data->course->unique_link }}">{{ $data->course->name }}</a></td>
                                <td><span class="pr-1">Course </span></td>
                            @endif

                        @elseif( $data->folder != null )

                            @if(!in_array($data->folder_id, $showCourseObject ))
                                <?php array_push($showCourseObject, $data->folder_id); ?>
                                <td><a href="/folder/ql/fo/{{ $data->folder->unique_link }}">{{ $data->folder->name }}</a></td>
                                <td><span class="pr-2">Folder </span></td>
                            @endif
                            
                        @elseif( $data->worksheet != null)

                            @if(!in_array($data->worksheet_id, $showCourseObject ))
                                <?php array_push($showCourseObject, $data->worksheet_id); ?>
                                <td><a href="/worksheet/ql/wo/{{ $data->worksheet->unique_link }}">{{ $data->worksheet->name }}</a></td>
                                <td><span class="pr-0">Worksheet </span> </td>
                            @endif

                        @endif

                        <?php $i++; ?>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>You have access to</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <!-- Pages specific scripts -->
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('#DataTables_Table').DataTable();
        });

    </script>
@stop