@extends('layouts.app')

@section('content')

<div class="ibox ">
    <div class="ibox-title">
        <h5>Create new worksheet</h5>
        <div class="ibox-tools">
            <a href="{{ URL::previous() }}" class="btn btn-primary btn-xs">Cancel</a>
        </div>

    </div>

    <div class="ibox-content">
          
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul class="mb-0">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }} 
        </div>
        @endif

        <div class="row">  

            {{ Form::open(array('route' => 'worksheet.store', 'class' => 'row' )) }}

            @csrf

            <div class="col-md-6 col-sm-12 col-xs-12">
                {!! Form::label('name', 'Worksheet title *', ['class' => 'control-label col-md-12 col-sm-3 col-xs-12']) !!}
                <div class="col-md-12 col-sm-6 col-xs-12">
                    {!! Form::text('name', '', ['id' => 'name', 'class' => 'form-control col-xs-12', 'required' => 'required']) !!}
                </div>
            </div>

            <!--
            <div class="col-md-6 col-sm-12 col-xs-12">
                {!! Form::label('name', 'Worksheet menu name *', ['class' => 'control-label col-md-12 col-sm-3 col-xs-12']) !!}
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::text('menu_name', '', ['id' => 'menu_name', 'class' => 'form-control col-xs-12', 'required' => 'required']) !!}
                </div>
            </div>
            -->
            
            <div class="col-md-12 col-sm-12 col-xs-12 mb-4 mt-4">
                {!! Form::label('name', 'Description *', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12']) !!}
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::textarea('description', '', ['id' => 'description', 'class' => 'form-control col-xs-12', 'rows' => '3', 'required' => 'required']) !!}
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12 mb-4">
                {!! Form::label('name', 'Which course?', ['class' => 'control-label col-md-12 col-sm-3 col-xs-12']) !!}
                <div class="col-md-12 col-sm-12 col-xs-12">
                    {!! Form::select('course_id', $course_list, null, array('id' => 'course_id', 'class' => 'form-control
                    col-xs-12', 'placeholder'=>'Select a option', 'onchange' => 'selectCourseFolder(this)')) !!}
                </div>
            </div>
            
            <div id="folder_wrapper" class="col-md-6 col-sm-12 col-xs-12 mb-4" style="display:none">
                {!! Form::label('name', 'Which folder?', ['class' => 'control-label col-md-12 col-sm-3 col-xs-12']) !!}
                <div class="col-md-12 col-sm-12 col-xs-12">
                <select name="folder_id" id="folder_object" class="form-control col-xs-12"></select>
                </div>
            </div>

            {!! Form::hidden('form_json', 'test', ['id' => 'form_json']) !!}

            {{ Form::close() }}
            
        </div>

        <!--Form Builder Wrapper-->
        <div id="fb-editor"></div>

    </div>
</div>

@endsection

@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>

<script>
$(function($) {

    // Options and settging for the form builder
    var options = {
        onOpenFieldEdit: function(editPanel) {
            // we need to hide the name field, it set, and should not be changed.
          $('.fld-name').parents('.name-wrap').hide()
        },
        typeUserDisabledAttrs: {
            'checkbox-group': [
                'description',
                'required',
                'toggle',
                'other'
                ],
            'paragraph': [
                'subtype'
                ],
            'text': [
                'subtype',
                'value',
            ],
            'textarea':[
                'subtype',
                'value',
            ],
            'radio-group':[
               
                'description',
                'required',
                'toggle',
                'other'
            ]
        },
        disableFields: [
            'autocomplete',
            'file',
            'date',
            'hidden',
            'number',
            'button'
        ],
        controlOrder: [
            'header',
            'paragraph',
            'text',
            'textarea',
            'checkbox-group',
            'radio-group',
            'select',
            'button'
        ],
        disabledAttrs: [
            'access',
            'className',
            'required',
            'description',
            'placeholder',
            //'name',
        ],
        disabledActionButtons: [
            'data'
        ],
        replaceFields: [{
                type: "checkbox-group",
                label: "Checkbox",
                values: [{
                    label: "label",
                    value: "value"
                }],
            },
            {
                type: "select",
                label: "Select",
                values: [{
                    label: "label",
                    value: "value"
                }],
            }
        ],
        onSave: function(evt, formData) {
            //call save data function
            saveDataForm(formData);
        }
    };

    
    // initializing form builder
    $('#fb-editor').formBuilder(options);

    function saveDataForm(data) {

        // init varibles
        //var getMenuName = $("#menu_name").val();
        //var getName = $("#name").val();
        //var getDescription = $("#description").val();

        document.getElementById('form_json').value = data;

        $("form").submit();

        /*
        $.ajax({
            method: "POST",
            url: "/worksheet",
            dataType: 'json',
            data: {
                _token: "{{ csrf_token() }}",
                menu_name: getMenuName,
                name: getName,
                description: getDescription,
                form_data: data
            },
            async: false,
            success: function(data) {
                $("#message").html(data.success);
                console.log(data);
            }
        });
        */
    }
});

function selectCourseFolder(elementObj){

    console.log(elementObj.value);

    if(elementObj.value != ""){
        var course_data = {!! json_encode($course) !!};

        $('#folder_wrapper').show();

        //folder_object clear html
        $('#folder_object').html('');

        // add folder child elements
        course_data[elementObj.selectedIndex - 1].folders.forEach(element => {
            daySelect = document.getElementById('folder_object');
            daySelect.options[daySelect.options.length] = new Option(element.name, element.id);
        });

        console.log(course_data);
    }else{
        $('#folder_wrapper').hide();
        $('#folder_object').html('');
    }

}

/**
 * remove the HTML on copy & paste from the paragraph input
 * we want plain text.
 */ 
function stripHTML(){
    const target = document.querySelector('div[contenteditable="true"]');
    // add event lister to the paste featue on the keyboard
    target.addEventListener('paste', (event) => {
        event.preventDefault();
        var text = event.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
    });
}

</script>

@stop