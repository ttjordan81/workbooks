@extends('layouts.app')

@section('content')

<div class="ibox ">
    <div class="ibox-title">
        <h5>View worksheet to be show to student</h5>
        <div class="ibox-tools">
            
            <!-- Button trigger modal -->
            <button type="button" data-toggle="modal" class="btn btn-primary btn-xs" data-target="#cancelModal">
                cancel edits
            </button>
            <button type="button" data-toggle="modal" class="btn btn-primary btn-xs" data-target="#deleteModal">
                [delete]
            </button>
        </div>

    </div>

    <div class="ibox-content">

            @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="mb-0">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif

            <div class="row">

                {{ Form::model($worksheet, ['route' => array('worksheet.update', $worksheet->id), 'id' => 'edit-worksheet', 'class' => 'row', 'method' => 'PUT'] ) }}


                @csrf

                <div class="col-md-6 col-sm-12 col-xs-12">
                    {!! Form::label('name', 'Worksheet title *', ['class' => 'control-label col-md-12 col-sm-3
                    col-xs-12']) !!}
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        {!! Form::text('name', $worksheet->name, ['id' => 'name', 'class' => 'form-control
                        col-xs-12', 'required' => 'required']) !!}
                    </div>
                </div>
                
                <!--
                <div class="col-md-6 col-sm-12 col-xs-12">
                    {!! Form::label('name', 'Worksheet menu name *', ['class' => 'control-label col-md-12
                    col-sm-3 col-xs-12']) !!}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::text('menu_name', $worksheet->menu_name, ['id' => 'menu_name', 'class' =>
                        'form-control col-xs-12', 'required' => 'required']) !!}
                    </div>
                </div>
                -->

                <div class="col-md-12 col-sm-12 col-xs-12 mb-4 mt-4">
                    {!! Form::label('name', 'Description *', ['class' => 'control-label col-md-3 col-sm-3
                    col-xs-12']) !!}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::textarea('description', $worksheet->description, ['id' => 'description',
                        'class' => 'form-control col-xs-12', 'rows' => '3', 'required' => 'required']) !!}
                    </div>
                </div>



                <div class="col-md-6 col-sm-12 col-xs-12 mb-4">
                    {!! Form::label('name', 'Which course?', ['class' => 'control-label col-md-12 col-sm-3
                    col-xs-12']) !!}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::select('course_id', $course_list, $worksheet->folder != null ? $worksheet->folder->course_id : '' , array('id' => 'course_id', 'class' =>
                        'form-control
                        col-xs-12', 'placeholder'=>'Select a option', 'onchange' => 'selectCourseFolder(this, null)'))
                        !!}
                    </div>
                </div>


                <div id="folder_wrapper" class="col-md-6 col-sm-12 col-xs-12 mb-4"  style="<?php echo $worksheet->folder == null ? 'display:none': ' '; ?> " >
                    {!! Form::label('name', 'Which folder?', ['class' => 'control-label col-md-12 col-sm-3
                    col-xs-12']) !!}
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        {!! Form::select('folder_id', $folder_list, null, array('id' => 'folder_object', 'class' => 'form-control
                        col-xs-12', 'placeholder'=>'Select a option')) !!}
                    </div>
                </div>


                {!! Form::hidden('form_json', 'test', ['id' => 'form_json']) !!}

                {{ Form::close() }}

            </div>

            <hr>
            <h5 class="mb-3">Form Builder <small>[ Drag and drop fields ]</small></h5>

            <!--Form Builder Wrapper-->
            <div id="fb-editor"></div>
        
    </div>
</div>








<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteModalLabel">Deleting Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Warning: You are about to delete this worksheet. This process is irreversible. Are you sure you want to
                delete this worksheet?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::model($worksheet, ['route' => array('worksheet.destroy', $worksheet->id), 'method' => 'DELETE'] ) }}
                <button type="submit" class="btn btn-danger">[Yes, I'm sure!]</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>


<!-- Delete Modal -->
<div class="modal fade" id="cancelModal" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancelModalLabel">Deleting Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Warning: Are you sure you want to cancel edit
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Continue to editing</button>
            <a href="{{ URL::previous() }}" class="btn btn-warning">Yes, I'm sure cancel</a>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-builder.min.js"></script>

<script>
$(function($) {

    // Options and settging for the form builder
    var options = {
        onOpenFieldEdit: function(editPanel) {
            // we need to hide the name field, it set, and should not be changed.
          $('.fld-name').parents('.name-wrap').hide()
        },
        typeUserEvents: {
            paragraph: {
                onadd: function(fld) {
                    stripHTML(); // call to function 
                }
            }
        },
        defaultFields: <?php echo $worksheet->form_json; ?> ,
        typeUserDisabledAttrs: {
            'checkbox-group': [
                'name',
                'description',
                'required',
                'toggle',
                'other'
                ],
            'paragraph': [
                'subtype'
                ],
            'text': [
                'subtype',
                'value',
            ],
            'textarea':[
                'subtype',
                'value',
            ],
            'radio-group':[
                'description',
                'required',
                'toggle',
                'other'
            ]
        },
        disableFields : [
            'autocomplete',
            'file',
            'date',
            'hidden',
            'number',
            'button',
        ],
        controlOrder: [
            'header',
            'paragraph',
            'text',
            'textarea',
            'checkbox-group',
            'radio-group',
            'select',
            'button'
        ],
        disabledAttrs: [
            'access',
            'className',
            'required',
            'description',
            'placeholder',
            //'name',
        ],
        disabledActionButtons: [
            'data'
        ],
        replaceFields: [{
                type: "checkbox-group",
                label: "Checkbox",
                values: [{
                    label: "label",
                    value: "value"
                }],
            },
            {
                type: "select",
                label: "Select",
                values: [{
                    label: "label",
                    value: "value"
                }],
            }
        ],

        onSave: function(evt, formData) {
            //call save data function
            saveDataForm(formData);
        },
        fieldRemoveWarn: true // defaults to false
    };


    // initializing form builder
    formBuilder = $('#fb-editor').formBuilder(options);


    // initializing select drop down
    //selectCourseFolder(elementObj)
});

function saveDataForm(data) {
    document.getElementById('form_json').value = data;
    $("#edit-worksheet").submit();
}

function selectCourseFolder(elementObj, data = null){

    console.log(elementObj.value);

    if(elementObj.value != ""){
        var course_data = {!! json_encode($course) !!};

        $('#folder_wrapper').show();

        //folder_object clear html
        $('#folder_object').html('');

        // add folder child elements
        for (let i = 0; i < course_data.length; i++) {
            if(course_data[i].id == elementObj.value){
                console.log(course_data[i]);

                var folderSelect = document.getElementById('folder_object');
                $.each(course_data[i].folders, function(index, value) { 
                    folderSelect.options[folderSelect.options.length] = new Option(value.name, value.id);
                });
            }
        }
        
        }else{
            $('#folder_wrapper').hide();
            $('#folder_object').html('');
        }
}

/**
 * remove the HTML on copy & paste from the paragraph input
 * we want plain text.
 */ 
function stripHTML(){
    const target = document.querySelector('div[contenteditable="true"]');
    // add event lister to the paste featue on the keyboard
    target.addEventListener('paste', (event) => {
        event.preventDefault();
        var text = event.clipboardData.getData("text/plain");
        document.execCommand("insertHTML", false, text);
    });
}


</script>

@stop