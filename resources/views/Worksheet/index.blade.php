@extends('layouts.app')

@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }} 
    </div>
@endif

<div class="ibox ">
    <div class="ibox-title">
        <h5>View worksheet to be show to student</h5>
        <div class="ibox-tools">
            <a href="/worksheet/create" class="btn btn-primary btn-xs"> Create worksheet </a>
        </div>

    </div>

    <div class="ibox-content">
        @if( $worksheets->isEmpty() == false)

        <div class="col-md-6">
            <table id="DataTables_Table" class="table table-bordered">

                <thead>
                    <tr>
                        <th>Folder	</th>
                        <th>Worksheet</th>
                    </tr>
                </thead>
                <tbody>
            
                @foreach($worksheets as $worksheet)
                    <tr>
                        <td> 
                            @if($worksheet->folder_id == null )
                                <a href="/worksheet/{{ $worksheet->id }}/edit">Assign to folder</a>
                            @else
                                {{ $worksheet->folder->name }}
                            @endif
                        </td>
                        <td>
                            <a href="/worksheet/{{ $worksheet->id }}">{{ $worksheet->name }}</a>
                        </td>
                     </tr>
                @endforeach
            </table>
        </div>
            
        @else
            @can('create', App\Course::class)
            No worksheet create yet!  <a href="/worksheet/create">Create new worksheet </a>
            @endcan
        @endif
    </div>
</div>


@endsection


@section('scripts')
    <!-- Pages specific scripts -->
    <script src="js/plugins/dataTables/datatables.min.js"></script>
    <script src="js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    
    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('#DataTables_Table').DataTable({
                pageLength: 10,
                responsive: true,
            });

        });

    </script>
@stop