@extends('layouts.app')

@section('content')

<div class="ibox ">
    <div class="ibox-title">
        <h5>View worksheet to be show to student</h5>
        <div class="ibox-tools">
   
            <a href="/worksheet-clone/{{ $worksheet->id }}" class="btn btn-primary btn-xs">Clone</a>
            <a href="/worksheet/{{ $worksheet->id }}/edit" class="btn btn-primary btn-xs">Edit</a>
            <a href="{{ URL::previous() }}" class="btn btn-primary btn-xs">Cancel</a>
        </div>

    </div>

    <div class="ibox-content">

        <h2>{{ $worksheet->name }}</h2>

        @if($worksheet->folder == null)
        <p>
            Before a worksheet can be shared, it needs to be <a href="/worksheet/{{ $worksheet->id }}/edit">assign to folder.</a>
        </p>
        @else
            @if(Auth::id() === $worksheet->creator_user_id || Auth::user()->hasRole('superadmin') )
            <p><b>Shared link:</b> <br>
                <input type="text" value="{{ url('worksheet/ql/wo') }}/{{ $worksheet->unique_link }}" id="myInput"
                    class="col-md-12 mb-2" />

                <a class="btn btn-success btn-sm" href="{{ url('worksheet/ql/wo') }}/{{ $worksheet->unique_link }} "
                    target="_blank"><i class="fa fa-link"></i> Review</a>

                <button class="btn btn-success btn-sm" onclick="myFunction()">Copy link</button>
            </p>
            @endif
        @endif


        @if(isset($worksheet->folder->name))
        <h5><div class="p-1 mb-1 bg-secondary text-white">Assign to folder: {{ $worksheet->folder->name }}</div></h5>
        @endif

        <p><b>Description</b><br>
        {{ $worksheet->description }}
        </p>

        <hr>
        
        {{ Form::open(array('route' => 'worksheet.store', 'class' => 'row' )) }}

        @csrf
        <div class="col-md-12">
        <!--Form show Wrapper-->
            <div id="fb-render" ></div>

            <button class="btn btn-primary" onclick="saveDataForm()" disabled>DISABLED</button>
        </div>
        {{ Form::close() }}

    </div>
</div>


@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script src="https://formbuilder.online/assets/js/form-render.min.js"></script>

<script>

function myFunction() {
    /* Get the text field */
    var copyText = document.getElementById("myInput");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
}



$(function($) {

    // load data
    var fbTemplate = <?php echo $worksheet->form_json; ?> ;

    // initializing form builder render
    $('#fb-render').formRender({
        dataType: 'json',
        formData:  fbTemplate
    });

    function saveDataForm(data) {
        $("form").submit();
    }

});
</script>

@stop