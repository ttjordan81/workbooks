@extends('layouts.app')
@section('content')


@if ($errors->any())
<div class="alert alert-danger">
    <ul class="mb-0">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

@if (session('success'))
<div class="alert alert-success">
    {{ session('success') }}
</div>
@endif

<div class="ibox ">
    <div class="ibox-title">
        <h5>Example view of shared worksheet</h5>
        <div class="ibox-tools">
            
            <a href="/worksheet-clone/{{ $worksheet->id }}" class="btn btn-primary btn-xs">Clone</a> 
                        
            @if(Auth::id() == $worksheet->user_id)
                <!-- Button trigger modal -->
                <button type="button" data-toggle="modal" data-target="#exampleModal" class="btn btn-primary btn-xs">
                    [delete]
                </button>
            @endif
        </div>

    </div>

    <div class="ibox-content">

        <div id="result" class="" role="alert"></div>

        <h1>{{ $worksheet->name }} </h1>

        <div class="row">
            <div class="col-md-12">
                <p>
                    <b>Description:</b><br>

                    {{ $worksheet->description }}
                    </p>
                    
                    <p>
                    <b>Folder:</b> <a href="{{ $worksheet->folder->id }}">{{ $worksheet->folder->name }}</a>
                </p>

                <hr>

            </div>

            {{ Form::model($worksheet, ['route' => array('worksheet-filled-out.store', $worksheet->id), 'id' => 'form_saved',  'method' => 'POST'] ) }}
        
            @csrf

            <div class="col-md-12 mb-4">
                <div class="form-group">
                    {{ Form::label('folder_id', 'Select project folder:') }}
                    {!! Form::select('folder_id', $folders, null, array('class' => 'form-control
                    col-xs-12', 'placeholder'=>'Select a option')) !!}
                </div>
            </div>

            {{ Form::hidden('worksheet_id', $worksheet->id) }}

            {{ Form::hidden('form_data', null, ['id' => 'form_data']) }}

            <div class="col-md-12">
                <!--Form show Wrapper-->
                <div id="fb-render"></div>
                @if(Auth::id() === $worksheet->creator_user_id || Auth::user()->hasRole('superadmin') )
                <button class="btn btn-primary" disabled="">DISABLED</button>
                @else
                <button id="submitBtn" class="btn btn-primary">Submit</button>
                @endif
            </div>

            {{ Form::close() }}
        </div>

    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Deleting Course</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Warning: You are about to delete this worksheet. This process is irreversible. Are you sure you want to
                delete this worksheet?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::model($worksheet, ['route' => array('worksheet-filled-out.destroy', $worksheet->id), 'method' => 'DELETE'] ) }}
                
                <button type="submit" class="btn btn-danger">[Yes, I'm sure!]</button>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script src="https://formbuilder.online/assets/js/form-render.min.js"></script>

<script>
$(function($) {

    // load data
    var fbTemplate = <?php echo $worksheet->form_json; ?> ;

    // initializing form builder render
    $('#fb-render-ll').formRender({
        dataType: 'json',
        formData: fbTemplate
    });

    // we pass the data from the PHP varible to the javascript varible
    var data_submitted = <?php echo $data_submitted; ?> ;

});

/*
This has been updated to use the new userData method available in formRender
*/
var getUserDataBtn = document.getElementById("get-user-data");
var fbRender = document.getElementById("fb-render");
var originalFormData =  <?php echo $worksheet->form_json; ?> ;

jQuery(function($) {
    var formData = JSON.stringify(originalFormData);
    $(fbRender).formRender({ formData });
    getUserDataBtn.addEventListener("click",() => {
        window.alert( window.JSON.stringify($(fbRender).formRender("userData")) );
    },false);
});

$("#submitBtn").click(function(){        
    $("#form_data").val(window.JSON.stringify($(fbRender).formRender("userData")));
    $("#form_saved").submit(); // Submit the form
});


</script>

@stop