<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorksheetFilledOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('worksheet_filled_out', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('worksheet_id');
            $table->unsignedBigInteger('creator_user_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('folder_id')->nullable();
            //$table->string('menu_name');
            $table->string('name');
            $table->string('unique_link')->nullable();
            $table->text('description');
            $table->text('form_data_serialize');
            $table->text('form_json');
            $table->timestamps();

            // Assigned foregin keys
            $table->foreign('worksheet_id')->references('id')->on('worksheets')->onDelete('cascade');
            $table->foreign('creator_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('worksheet_filled_out');
    }
}
