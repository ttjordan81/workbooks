<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnrollmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollment', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('course_id')->nullable()->unsigned();
            $table->unsignedBigInteger('folder_id')->nullable()->unsigned();
            $table->unsignedBigInteger('worksheet_id')->nullable()->unsigned();
            $table->unsignedBigInteger('worksheet_filled_id')->nullable()->unsigned();
			$table->unsignedBigInteger('user_id')->nullable()->unsigned();
			$table->unsignedBigInteger('enrollment_status_id')->nullable()->unsigned();

            // Assigned foregin keys
            $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');
            $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('enrollment_status_id')->references('id')->on('enrollment_status')->onDelete('cascade');
            $table->foreign('worksheet_id')->references('id')->on('worksheets')->onDelete('cascade');
            $table->foreign('worksheet_filled_id')->references('id')->on('worksheet_filled_out')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrollment');
    }
}
