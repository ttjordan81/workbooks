<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User as User;

class RolesAndPermissionsSeeder extends Seeder
{
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();


        // create roles and assign created permissions

        // this can be done as separate statements
        Role::create(['name' => 'superadmin']);
        Role::create(['name' => 'teacher']);
        Role::create(['name' => 'student']);

        // Adding permissions via a role
        User::find(1)->assignRole('superadmin');
        User::find(2)->assignRole('teacher'); // User 2 has two roles teacher & student
        User::find(2)->assignRole('student'); // User 2 has two roles teacher & student
        User::find(3)->assignRole('teacher');
        User::find(4)->assignRole('student');
        User::find(5)->assignRole('student');
    }
}