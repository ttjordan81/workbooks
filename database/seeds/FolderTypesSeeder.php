<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User as User;

class FolderTypesSeeder extends Seeder
{
    public function run()
    {
        DB::table('folder_types')->delete();

		$enrollment = [
            ['id' => 1, 'name' => 'course_folders', 'slug' => 'course_folders'],
            ['id' => 2, 'name' => 'project_folders', 'slug' => 'project_folders']
            ];
            
        DB::table('folder_types')->insert($enrollment);
    }
}