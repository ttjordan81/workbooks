<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('users')->delete();

        // seeding database with starter users
        DB::table('users')->insert([
            [
                'name' => 'superadmin',
                'email' => 'superadmin@test.com',
                'password' => bcrypt('password'),
            ],[
                'name' => 'teacher',
                'email' => 'teacher@test.com',
                'password' => bcrypt('password'),
            ],[
                'name' => 'teacher2',
                'email' => 'teacher2@test.com',
                'password' => bcrypt('password'),
            ],[
                'name' => 'student',
                'email' => 'student@test.com',
                'password' => bcrypt('password'),
            ],[
                'name' => 'student2',
                'email' => 'student2@test.com',
                'password' => bcrypt('password'),
            ]
        ]);
    }
}
